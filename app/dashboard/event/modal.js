 const d = document;
 
 export function showModal(btnModal) {
    d.addEventListener("click", (e) => {
      if (e.target.matches(btnModal)) {
        d.querySelector(".container-modal").classList.toggle(
          "container-modal--active"
        );
         d.querySelector(".Window-modal").classList.toggle(
          "Window-modal--active"
        );
      }
    }); 
};


export function closeModalWindonw(closeModal) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(closeModal)) {
      d.querySelector(".Window-modal").classList.toggle(
        "Window-modal--active"
      );
       setTimeout(() => {
        d.querySelector(".container-modal").classList.toggle(
          "container-modal--active"
        );
      }, 500); 
    }
  });
}
