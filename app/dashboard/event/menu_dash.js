const d = document,
$dashMenu = d.querySelector('.menu-d__nav'),
$ul = d.querySelectorAll("ul");

export function menuDashboard(buttonMenu) {
    d.addEventListener("click", (e) => {
      if (e.target.matches(`${buttonMenu} *`)) {
        $dashMenu.classList.toggle('menu-d__nav-active');

          $ul.forEach((el) => {
            if (el.className.match('.menu-d__sub-menu--active')) {
                el.classList.remove('menu-d__sub-menu--active')
            }
          });  
      
      }
    });


  }

  export function subMemuDashboard(){
    d.addEventListener('click',(e)=>{
        
            if(e.target.matches('.menu-d__link')){
                
                let sibling = e.path[1].nextElementSibling; 
        
                  if(sibling === null){
                   return
                 }else{
                    e.preventDefault();
                    sibling.classList.toggle('menu-d__sub-menu--active');
           
                 } 
                }

            if(e.target.matches('.menu-d__icon')){
                 
                let sibling = e.path[2].nextElementSibling; 
               
                 if(sibling === null){
                   return
                 }else{

                   e.preventDefault();
                    sibling.classList.toggle('menu-d__sub-menu--active');
           
                 }  
            }

        
    })
  }
  
 