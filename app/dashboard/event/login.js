
export function showPasw(eyeBtn) {
    document.addEventListener("click", (e) => {
      if (e.target.matches(eyeBtn)) {
        if (e.path[1].nextElementSibling.type === "password") {
          e.path[1].nextElementSibling.type = "text";
          e.path[0].classList.remove("icon-eye-off");
          e.path[0].classList.add("icon-eye");
        } else {
          e.path[1].nextElementSibling.type = "password";
          e.path[0].classList.add("icon-eye-off");
          e.path[0].classList.remove("icon-eye");
        }
      }
    });
  };

  
