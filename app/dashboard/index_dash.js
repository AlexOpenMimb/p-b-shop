
//import { getFile } from "./event/drag-drop.js";
import { showPasw } from "./event/login.js";
import { menuDashboard, subMemuDashboard } from "./event/menu_dash.js";
import { closeModalWindonw, showModal } from "./event/modal.js";
import { create_user_validate } from "./validate/form-create-user.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
    showPasw('#btn-eye');
    create_user_validate();
    menuDashboard(".btn-dash");
    subMemuDashboard();
   showModal('#btn-categorie');
   showModal('#btn-user');
   closeModalWindonw('.close-modal');
});

