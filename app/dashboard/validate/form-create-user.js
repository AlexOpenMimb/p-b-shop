const d = document;
const $inputName = d.getElementById('login_name');
const $inputEmail = d.getElementById('login_email');
const $inputPasword = d.getElementById('input-pasword');
const $btnCreateLogin = d.getElementById('btn-login');
const $spanErrorName = d.getElementById('msm-error-name');
const $spanErrorEmail = d.getElementById('msm-error-email');
const $spanErrorPasw = d.getElementById('msm-error-password');
const expRMail = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

let stateBtn = {
    name:false,
    email:false,
    password:false
};
    

export function create_user_validate(){
    d.addEventListener('click',(e) => {
        if(e.target.matches('#btn-login')){

            validateName();

            validateEmail();

            validatePasword();

            changebtn();
        }
    } )
};

function validateName(){

    let value = $inputName.value;

    if(value === "" || value === " "){
        stateBtn.name = false;
        $spanErrorName.textContent = "No has ingresado el nombre"
    }else{

        stateBtn.name = true;
        $spanErrorName.textContent = ""  
    }
}

function validateEmail(){

     let value = $inputEmail.value;
     
     if(value === " "){
        stateBtn.email = false;
    $spanErrorEmail.textContent = "No has ingresado un email"
        
    }else if(!expRMail.test(value)){
        stateBtn.email = false;
    $spanErrorEmail.textContent = "No has ingresado un email valido"
     
    }else{  

    stateBtn.email = true;
    $spanErrorEmail.textContent = ""     
  

    } 
                
}

function validatePasword(){
    let value = $inputPasword.value;
   

     if(value.length < 8){
        stateBtn.password = false;

        $spanErrorPasw.classList.add('c-red'); 
        
    } else{
        stateBtn.password = true;
        $spanErrorPasw.classList.remove('c-red');

    }
}


function changebtn(){
    if(stateBtn.name && stateBtn.email && stateBtn.password){
        console.log(true)
        $btnCreateLogin.type = "submit";
    }else{
        console.log(false)
        $btnCreateLogin.type = "button";
    }
}