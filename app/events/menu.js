const d = document,
  $ul = d.querySelectorAll("ul");
 

export default function hamburguerMenu(buttonMenu, menu) {
  d.addEventListener("click", (e) => {
    if (e.target.matches(`${buttonMenu} *`)) {
      e.path[1].firstElementChild.classList.toggle("d-none");
      e.path[1].lastElementChild.classList.toggle("d-none");

      d.querySelector(menu).classList.toggle("menu--active");
      $ul.forEach((el) => {
        if (el.className === "sub-menu--active") {
          el.classList.remove("sub-menu--active");
          el.classList.add("sub-menu--unactive");
        }
      });
    }
  });
}

export function subMenu(widt,f_link,f_i,s_link,s_i,t_link,t_i) {
  d.addEventListener("click", (e) => {
      
      if (window.innerWidth < widt) {
    
      if(e.target.matches(f_link)){
         
        let sibling = e.path[f_i].nextElementSibling; 

          if(sibling === null){
           return
         }else{
          console.log(e.target)
          let  menuActive = sibling.className,
         handleMenu = sibling.classList;
         
         expandMenu(menuActive,handleMenu,e,"sub-menu--active","sub-menu--unactive")
       
   
         } 
        }


        if(e.target.matches(s_link)){

          let sibling = e.path[s_i].nextElementSibling; 

          if(sibling === null){
           return
         }else{
          console.log(e.target)
          let  menuActive = sibling.className,
         handleMenu = sibling.classList;
        
         expandMenu(menuActive,handleMenu,e,"sub-menu--active","sub-menu--unactive")
       
          
         } 
        }

         if (e.target.matches(t_link)) {
          
          let sibling = e.path[t_i].nextElementSibling;

        if(sibling === null){
         return
       }else{

        let  menuActive = sibling.className,
       handleMenu = sibling.classList;

       expandMenu(menuActive,handleMenu,e,"sub-menu--active","sub-menu--unactive")
        
       } 
     } 
      
    }
  });
}

function expandMenu (menu,handM,even,menuActive,menuUnactive){
  even.preventDefault();
  if (menu === menuUnactive) {
    handM.remove(menuUnactive);
    handM.add(menuActive);
  } else {
    handM.remove(menuActive);
    handM.add(menuUnactive);
  }
}


